$(document).ready(function() {
  
  var Visitors = function(){
    var set = {}
    // hide or show necessary color 
    set.check_rows = function(id) {
      $('tbody').children('tr').each(function() {
        if ($(this).data('color') !== id) {
          $(this).hide();
        } else {
          $(this).show();
        }
      });
    };

    // make danger column
    set.make_danger = function() {
      $('table  .time_end').each(function(){
        if($(this).data("time-end") == true) $(this).closest('tr').addClass('danger')
      });
    }

    set.get_price = function(){
      // show price of type
      $('#visitor_type_id').change(function() {
        var id;
        id = $(this).val();
        if (id > 0) {
          $.ajax({
            type: 'get',
            url: '/types/' + id + '.json',
            success: function(d) {
              if (typeof d === 'object') {
                $('.price').text(d.price);
                $('.pay').show('slow');
              }
            }
          });
        }
      });
    }    

    set.clicker_function = function(){
      // if clicked by color show only this color
      $('.color-box').click(function() {
        var c_id;
        c_id = $(this).data('id');
        set.check_rows(c_id);
      });
    }

    set.init = function(){
      set.clicker_function()
      set.get_price()
      set.make_danger()
    }
    set.init();

  return set;
  }();
    //seach
    $('#search_form').submit(function() {
      $.get(this.action, $(this).serialize(), null, 'script');
      return false;
    });

  });