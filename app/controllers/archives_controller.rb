class ArchivesController < ApplicationController
	before_action :check_rules

  def index
    # @visitors = Visitor.unscoped.with_deleteds
    @date = params[:month] ? Date.parse(params[:month]) : Date.today
  end

  def detail
   @date = params[:date].to_date
   @visitors = Visitor.unscoped.where(created_at: @date..@date+1.day).includes(:type, :user)
   @c = Visitor.unscoped.where(created_at: @date..@date+1.day).includes(:type, :user).sum('types.price')
    
    respond_to do |format|
      format.html {}
      format.csv  { send_data @visitors.as_csv, filename: "visitors-#{@date}.csv" }
    end
  end

  private 
  	def check_rules
  		redirect_to root_path unless current_user.any_role?(archives: [:datail, :index])
  	end
end
