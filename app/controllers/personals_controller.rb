class PersonalsController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :check_rules  

  def index
    @users = User.includes(:role).order(id: :desc)
    respond_to do |format|
      format.html {}
      format.json {@users.to_json}
    end
  end

  def show
    respond_to do |format|
      format.html {}
      format.json {@user.to_json}
    end
  end

  def new
    @personal = User.new
  end

  def edit
  end

  def create
    @personal = User.new(user_params)
    
    if @personal.save
      respond_to do |format|
        format.html { redirect_to personals_path}
        format.js {}
      end
    else
      respond_to do |format|
        format.html { render "new" }
        format.js {}
      end
    end
  end

  def update
    if @personal.update(user_params)
      respond_to do |format|
        format.html { redirect_to personals_path}
        format.js {}
      end
    else
      respond_to do |format|
        format.html {render "edit"}
        format.js {}
      end
    end
  end

  def destroy
    @personal.destroy
    redirect_to personals_path
  end

  private
    def set_user
      @personal = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:name, :email, :password, :role_id)
    end

    def check_rules
      redirect_to root_path unless current_user.admin?
    end
end
