class ReportsController < ApplicationController
  before_action :check_rules 
  
  def index
  	if params[:dates]
  		from = params[:dates][:from].to_date
  		to = params[:dates][:to].to_date
  		@visitors = Visitor.unscoped.where(created_at: from..to)

			send_data @visitors.as_csv, filename: "visitors-#{Date.today}.csv" 	    
	  end 
  end
 	
 	def check_rules
      redirect_to root_path unless current_user.admin?
  end
end
