class TypesController < ApplicationController
  before_action :set_type, only: [:show, :edit, :update, :destroy]
  before_action :check_rules, except: [:show]  

  def index
    @types = Type.all
    respond_to do |format|
      format.html {}
      format.json {@types.to_json}
    end
  end

  def show
    respond_to do |format|
      format.html {}
      format.json {@type.to_json}
    end
  end

  def new
    @type = Type.new    
  end

  def edit
  end

  def create
    @type = Type.new(type_params)
    if @type.save
      redirect_to types_path
    else
      render "new"
    end
    
  end

  def update
    @type.update(type_params)
    redirect_to types_path
  end

  def destroy
    @type.destroy
    redirect_to types_path
  end

  private
    def set_type
      @type = Type.find(params[:id])
    end

    def type_params
      params.require(:type).permit(:color, :time, :price, :name)
    end

    def check_rules
      redirect_to root_path unless current_user.admin? || current_user.moderator?
    end
end
