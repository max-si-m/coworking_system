class VisitorsController < ApplicationController
  before_action :set_visitor, only: [:show, :edit, :update, :destroy]

  require 'csv'

  def index
    if current_user.admin? || current_user.moderator?(:roles)
        @visitors = Visitor.includes(:type).search(params[:search])
    else        
        @visitors = current_user.visitors.includes(:type).search(params[:search])
    end

    @date = params[:month] ? Date.parse(params[:month]) : Date.today
    
    @types = Type.order(id: :desc)
    @c_visitors = Visitor.with_deleted.count

    respond_to do |format|
      format.html {}
      format.js   {}
      format.csv  { send_data @visitors.as_csv, filename: "visitors-#{Date.today}.csv" }
    end
  end

  def show
    respond_to do |format|
      format.html {}
      format.js {}
    end
  end

  def new
    @visitor = Visitor.new

    respond_to do |format|
      format.html {}
      format.js {}
    end
  end

  def edit
    redirect_to root_path unless current_user.admin? || current_user.moderator?
  end

  def create
    @visitor = current_user.visitors.create(visitor_params)

    if @visitor.save
      respond_to do |format|
        format.html { redirect_to visitors_path}
        format.js {}
      end
    else
      respond_to do |format|
        format.html { render "new" }
        format.js {}
      end
    end

  end

  def update
    @visitor.update(visitor_params)

    respond_to do |format|
      format.html { redirect_to root_path}
      format.js {}
    end
  end

  def destroy
    @visitor.destroy
    respond_to do |format|
      format.html { redirect_to visitors_path}
      format.js {}
    end
  end

  private
    def set_visitor
      @visitor = Visitor.find(params[:id])
    end

    def visitor_params
      params.require(:visitor).permit(:name, :phone_number, :type_id)
    end
end
