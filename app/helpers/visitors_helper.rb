module VisitorsHelper
	def color_of_type(type)
		tag("span", style: "background: #{type.color};", class: "color-box", data:{id:type.id})
	  # content_tag(:span, style: "background: #{color};", :class => "color-box")
	end
  def check_time(visitor)
    valid_date = visitor.end_time.strftime("%H:%M")
    if DateTime.now.to_i >= visitor.end_time.to_i
      content_tag(:span, valid_date, class: ["time_end"], data:{time_end: true})
    else
      valid_date
    end
  end
end
