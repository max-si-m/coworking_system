# == Schema Information
#
# Table name: roles
#
#  id          :integer          not null, primary key
#  name        :string(255)      not null
#  title       :string(255)      not null
#  description :text             not null
#  the_role    :text             not null
#  created_at  :datetime
#  updated_at  :datetime
#

class Role < ActiveRecord::Base
  include TheRole::Api::Role
end
