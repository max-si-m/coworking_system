# == Schema Information
#
# Table name: types
#
#  id           :integer          not null, primary key
#  time         :integer
#  color        :string(255)
#  price        :integer
#  name         :string(255)
#  created_at   :datetime
#  updated_at   :datetime
#  time_seconds :integer
#

class Type < ActiveRecord::Base
	validates :time, numericality: { greater_than_or_equal_to: 1 }
	before_save :convert2seconds

	belongs_to :visitor

	protected
		def convert2seconds
			self.time_seconds = self.time * 3600
		end
end
