# == Schema Information
#
# Table name: visitors
#
#  id         :integer          not null, primary key
#  time       :datetime
#  name       :string(255)
#  type_id    :integer
#  user_id    :integer
#  created_at :datetime
#  updated_at :datetime
#  end_time   :datetime
#

class Visitor < ActiveRecord::Base
  acts_as_paranoid
	validates :time, presence: true
	validates :name, presence: true
	validates :user, presence: true
	validates :type, presence: true

	default_scope { where("created_at > ?", DateTime.now.midnight).order(type_id: :asc) }

	before_validation :add_time, on: :create
	before_save :count_time2end
	before_update :count_time2end
	
	belongs_to :type
	belongs_to :user

	def self.as_csv
	  CSV.generate do |csv|
      csv << ["Time", "End Time", "Name", "Phone number","Administrator name", "Type"] 
      sum = 0
      all.each do |vis|
        sum += vis.type.price
        csv << [self.to_correct_date(vis.time), self.to_correct_date(vis.end_time), vis.name, vis.phone_number, vis.user.name, vis.type.name]
      end
      csv << ["\n"]
      csv << ["Summ", sum]
	  end
	end

	def self.to_correct_date(date)
		date.strftime("%d-%m-%Y %H:%M")
	end

	def self.search(search)
	  if search
	    where('lower(name) LIKE lower(?)', "%#{search}%")
	  else
	    all
	  end
	end

	protected
		def add_time
			self.time ||= DateTime.now()
		end

		def count_time2end
			last_time = Time.now().end_of_day - 7200
			e_time = self.time + self.type.time_seconds
			
			self.end_time ||= ( e_time > last_time) ? last_time : e_time			
		end
end
