json.array!(@types) do |type|
  json.extract! type, :id, :time, :color, :price, :name
  json.url type_url(type, format: :json)
end
