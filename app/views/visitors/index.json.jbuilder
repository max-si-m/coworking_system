json.array!(@visitors) do |visitor|
  json.extract! visitor, :id, :time, :name, :type_id, :user_id
  json.url visitor_url(visitor, format: :json)
end
