Rails.application.routes.draw do
 
  match 'reports', to: 'reports#index', via: [:get, :post]

  get 'archives/detail/:date', to: 'archives#detail', as: 'archives'
  get 'archives/index', to: 'archives#index'

  resources :types
  resources :visitors
  resources :personals
  
  devise_for :users

  TheRoleManagementPanel::Routes.mixin(self)
  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'visitors#index'
end
