class CreateTypes < ActiveRecord::Migration
  def change
    create_table :types do |t|
    	t.integer :time  #time in timestap to end
    	t.string 	:color #RGB color 
    	t.integer :price #in grivna
    	t.string  :name  #name of type 

      t.timestamps
    end
  end
end
