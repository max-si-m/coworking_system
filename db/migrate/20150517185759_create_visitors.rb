class CreateVisitors < ActiveRecord::Migration
  def change
    create_table :visitors do |t|
      t.datetime :time
      t.string :name
      t.integer :type_id
      t.integer :user_id

      t.timestamps
    end
  end
end
