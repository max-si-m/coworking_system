class AddTimeSecondsToType < ActiveRecord::Migration
  def change
    add_column :types, :time_seconds, :integer
  end
end
