class AddEndTimeToVisitors < ActiveRecord::Migration
  def change
    add_column :visitors, :end_time, :datetime
  end
end
