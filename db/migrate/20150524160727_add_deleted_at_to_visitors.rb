class AddDeletedAtToVisitors < ActiveRecord::Migration
  def change
    add_column :visitors, :deleted_at, :datetime
    add_index :visitors, :deleted_at
  end
end
