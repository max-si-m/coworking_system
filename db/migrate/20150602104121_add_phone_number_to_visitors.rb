class AddPhoneNumberToVisitors < ActiveRecord::Migration
  def change
    add_column :visitors, :phone_number, :string
  end
end
