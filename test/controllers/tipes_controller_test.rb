require 'test_helper'

class TipesControllerTest < ActionController::TestCase
  setup do
    @tipe = tipes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tipes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tipe" do
    assert_difference('Tipe.count') do
      post :create, tipe: { color: @tipe.color, name: @tipe.name, price: @tipe.price, time: @tipe.time }
    end

    assert_redirected_to tipe_path(assigns(:tipe))
  end

  test "should show tipe" do
    get :show, id: @tipe
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tipe
    assert_response :success
  end

  test "should update tipe" do
    patch :update, id: @tipe, tipe: { color: @tipe.color, name: @tipe.name, price: @tipe.price, time: @tipe.time }
    assert_redirected_to tipe_path(assigns(:tipe))
  end

  test "should destroy tipe" do
    assert_difference('Tipe.count', -1) do
      delete :destroy, id: @tipe
    end

    assert_redirected_to tipes_path
  end
end
